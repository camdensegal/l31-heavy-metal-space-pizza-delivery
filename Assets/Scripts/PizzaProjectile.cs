﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody2D))]
public class PizzaProjectile : MonoBehaviour {
	private Rigidbody2D rb;

	[Range (0, 300)]
	public float rotationMax = 30;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();

		rb.angularVelocity = Random.Range( - rotationMax, rotationMax );
	}

	void OnCollisionEnter2D( Collision2D col ) {
		Debug.Log( "PIZZA HIT: " + col.transform.root.name );
	}
}
