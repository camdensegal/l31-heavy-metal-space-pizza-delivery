﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

	public int pizzasOrdered = 1;

	private DeliveryManager dm;

	void Start() {
		dm = FindObjectOfType<DeliveryManager>();
	}

	void OnCollisionEnter2D( Collision2D col ) {
		PizzaProjectile pizza = col.collider.GetComponent<PizzaProjectile>();
		if ( pizza && pizzasOrdered > 0) {
			Destroy( col.gameObject );
			Debug.Log( "PIZZA DELIVERED" );
			dm.PizzaDelivered();
			pizzasOrdered --;
		}
	}
}
