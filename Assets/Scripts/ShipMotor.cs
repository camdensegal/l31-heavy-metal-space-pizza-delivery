﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
public class ShipMotor : MonoBehaviour {

	[Range (0, 1000)]
	public float power = 1;

	[Range (0, 1000)]
	public float turnSpeed = 1;

	public float Angle {
		get {
			return tf.eulerAngles.z;
		}
	}

	private Rigidbody2D rb;
	private Transform tf;

	public bool on = true;

	private bool prevEngineFiring = false;
	public bool engineFiring = false;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		tf = GetComponent<Transform>();

		GetComponent<PizzaDeliveryDieOnCollision>().shipCrash += Crashed;
	}

	public void TurnTowards( Vector3 position ) {
		if ( on ) {
			position.z = tf.position.z; // Make sure positions are on same plane

			// Get direction to point
			Vector3 direction = (position - tf.position).normalized;

			// the vector that we want to measure an angle from
			Vector3 referenceForward = tf.up;/* some vector that is not Vector3.up */
				
			// the vector perpendicular to referenceForward (90 degrees clockwise)
			// (used to determine if angle is positive or negative)
			Vector3 referenceRight = - tf.right;
				
			// Get the angle in degrees between 0 and 180
			float angle = Vector3.Angle(direction, referenceForward);
			
			// Determine if the degree value should be negative.  Here, a positive value
			// from the dot product means that our vector is on the right of the reference vector   
			// whereas a negative value means we're on the left.
			float sign = Mathf.Sign(Vector3.Dot( direction, referenceRight) );
			
			angle *= sign;

			// Make sure angle is between -1 and 1
			angle = Mathf.Max( -1, Mathf.Min( 1, angle / 90 ) );

			// Instantanious torque
			rb.angularVelocity =  angle * turnSpeed;
		}
	}

	public void FireEngine( float throttle ) {
		if ( on ) {
			// Make sure throttle is between 0 and 1
			throttle = Mathf.Max( 0, Mathf.Min( 1, throttle ) );

			throttle = throttle * power * Time.deltaTime;

			rb.AddRelativeForce( new Vector2( 0, throttle ) );

			engineFiring = true;
			prevEngineFiring = true;
		}
	}

	void Update() {
		if ( prevEngineFiring == false ) {
			engineFiring = false;
		} else {
			prevEngineFiring = false;
		}
	}

	private void Crashed( Collision2D col ) {
		on = false;
	}
}
