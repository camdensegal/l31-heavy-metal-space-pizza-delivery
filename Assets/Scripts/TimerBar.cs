﻿using UnityEngine;
using System.Collections;

public class TimerBar : MonoBehaviour {

	public TimeManager tm;
	private GUIBar bar;

	void Start () {
		bar = GetComponent<GUIBar>();
	}

	void Update () {
		bar.Fill = tm.TimeLeft / tm.timeLimit;
	}
}
