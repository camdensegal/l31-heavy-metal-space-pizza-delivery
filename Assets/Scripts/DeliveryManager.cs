﻿using UnityEngine;
using System.Collections;

public class DeliveryManager : MonoBehaviour {


	private int deliveriesLeft = 0;

	public delegate void deliveryFinishedHandler();
	public event deliveryFinishedHandler DeliveriesFinished;

	// Use this for initialization
	void Start () {
		Target[] targets = FindObjectsOfType<Target>();

		for ( int i = 0; i < targets.Length; i ++ ) {
			deliveriesLeft += targets[i].pizzasOrdered;
		}
	}
	
	public void PizzaDelivered() {
		deliveriesLeft--;
		Debug.Log( "Deliveries Left: " + deliveriesLeft );
		if ( deliveriesLeft == 0 ) {
			Debug.Log("Delivered All Pizzas!");
			if ( DeliveriesFinished != null ) {
				DeliveriesFinished();
			}
		}
	}
}
