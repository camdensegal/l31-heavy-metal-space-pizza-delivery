﻿using UnityEngine;
using System.Collections;

public class RestartLevelButton : MonoBehaviour {
	public string restartButton = "Restart";

	void Update() {
		if ( Input.GetButtonDown( restartButton ) ) {
			RestartLevel();
		}
	}

	public void RestartLevel() {
		Application.LoadLevel( Application.loadedLevel );
	}
}
