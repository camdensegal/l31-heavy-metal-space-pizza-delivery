﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(Rigidbody2D))]
public class ObstacleMovement : MonoBehaviour {

	public Vector2 movement;
	private Rigidbody2D rb;

	public float reverseAmount = 5;



	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();

		rb.velocity = GetComponent<Transform>().TransformDirection( movement );
	}

	void OnTriggerEnter2D( Collider2D collider ) {
		Transform tf = GetComponent<Transform>();

		tf.position += tf.TransformDirection( movement.normalized ) * - reverseAmount;
	}
}
