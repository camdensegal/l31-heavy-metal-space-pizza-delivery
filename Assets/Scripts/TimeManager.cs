﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

	[ Range( 0, 20 ) ]
	public float timeLimit;
	private float endTime = -1;

	private static TimeManager _instance;
	public static TimeManager Instance {
		get {
			if ( null == _instance ) {
				_instance = FindObjectOfType<TimeManager>();
			}
			return _instance;
		}
	}

	public float TimeLeft {
		get {
			if ( endTime == -1 ) {
				return timeLimit;
			} else {
				return endTime - Time.time;
			}
		}
	}

	public bool TimeIsUp {
		get {
			return ( endTime > 0 && Time.time > endTime );
		}
	}

	public delegate void TimeUpHandler();
	public event TimeUpHandler TimeUp;

	void Start() {
		_instance = this;
		//StartTimer ();
	}

	public void StartTimer() {
		if ( endTime == -1 ) {
			endTime = Time.time + timeLimit;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if ( TimeIsUp ) {
			Debug.Log( "TIME IS UP" );
			if ( TimeUp != null ) {
				TimeUp();
			}
			enabled = false;
		}
	}
}
