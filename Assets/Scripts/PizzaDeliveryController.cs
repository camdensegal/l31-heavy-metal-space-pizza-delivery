﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (ShipMotor))]
public class PizzaDeliveryController : MonoBehaviour {

	private ShipMotor sm;
	private Transform tf;

	public Camera cam;
	private CameraShake shake;
	public float dragDistance;

	[Range (0, 20)]
	public float fuel = 10;

	// Use this for initialization
	void Start () {
		sm = GetComponent<ShipMotor>();
		tf = GetComponent<Transform>();
		shake = cam.GetComponent<CameraShake>();
	}
	
	// Update is called once per frame
	void Update () {
		if ( fuel > 0 && sm.on ) {
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = tf.position.z - cam.GetComponent<Transform>().position.z;
			
			mousePos = cam.ScreenToWorldPoint( mousePos );
			
			sm.TurnTowards( mousePos );

			if( Input.GetMouseButton( 0 ) ) {
				float distance = Vector3.Distance( tf.position, mousePos );
				float throttle = distance / dragDistance;


				sm.FireEngine( throttle );
				shake.ShakeCamera( 0.01f, 0.2f );
				fuel -= throttle * Time.deltaTime;

				TimeManager.Instance.StartTimer();
			}
		} else {
			sm.on = false;
		}
	}


}
