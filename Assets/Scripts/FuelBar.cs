﻿using UnityEngine;
using System.Collections;

public class FuelBar : MonoBehaviour {
	
	public PizzaDeliveryController pdc;
	private GUIBar bar;

	private float startFuel;
	
	void Start () {
		bar = GetComponent<GUIBar>();

		startFuel = pdc.fuel;
	}
	
	void Update () {
		bar.Fill = pdc.fuel / startFuel;
	}
}
