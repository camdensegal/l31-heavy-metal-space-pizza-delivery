﻿using UnityEngine;
using System.Collections;

public class PizzaDeliveryDieOnCollision : MonoBehaviour {

	public delegate void ShipCrashHandler(Collision2D col);
	public event ShipCrashHandler shipCrash;

	public AudioClip crashSound;

	public Transform crashEffect;

	void OnCollisionEnter2D(Collision2D col) {
		Debug.Log( "SHIP CRASHED" );
		Camera.main.GetComponent<CameraShake>().ShakeCamera( 0.5f, 0.5f );
		if ( shipCrash != null) {
			shipCrash( col );
		}

		for ( int i = 0; i < col.contacts.Length; i ++ ) { 
			Instantiate( crashEffect, col.contacts[i].point, Quaternion.identity );
		}

		if ( crashSound ) {
			AudioSource.PlayClipAtPoint( crashSound, Vector3.zero );
		}
	}
}
