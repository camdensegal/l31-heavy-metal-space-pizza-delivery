﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class WinLoseManager : MonoBehaviour {

	private TimeManager timer;
	private DeliveryManager delivery;

	private bool won = false;

	public GameObject winPanel;
	public GameObject losePanel;
	public Text scoreText;

	// Use this for initialization
	void Start () {
		timer = GetComponent<TimeManager>();
		delivery = GetComponent<DeliveryManager>();

		timer.TimeUp += HandleTimeUp;
		delivery.DeliveriesFinished += HandleDeliveriesFinished;
	}

	void HandleDeliveriesFinished (){
		if ( ! timer.TimeIsUp ) {
			Debug.Log( "You win this round" );
			Debug.Log( "Load next level or win screen!" );
			won = true;
			scoreText.text = ( timer.timeLimit - timer.TimeLeft ).ToString("F");
			winPanel.SetActive( true );
			EventSystem.current.SetSelectedGameObject( winPanel.GetComponentInChildren<Button>().gameObject );
		}
	}

	void HandleTimeUp (){
		if ( ! won ) {
			Debug.Log( "You lose this round, restart the level" );
			losePanel.SetActive( true );
			EventSystem.current.SetSelectedGameObject( losePanel.GetComponentInChildren<Button>().gameObject );
		}
	}
}
