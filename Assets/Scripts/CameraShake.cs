﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraShake : MonoBehaviour {
	
	private struct Shake {
		public float intensity;
		public float duration;
		public float startTime;
	}
	
	private Vector3 shakeOffset = Vector3.zero;
	
	private List<Shake> shakes = new List<Shake>();
	
	public void ShakeCamera(float nIntensity, float nDuration) {
		Shake newShake = new Shake();
		newShake.intensity = nIntensity;
		newShake.duration = nDuration;
		newShake.startTime = Time.time;
		shakes.Add( newShake );
	}
	
	void Update() {
		float totalIntensity = 0;
		
		foreach ( Shake shake in shakes ) {
			if ( Time.time - shake.startTime < shake.duration ) {
				totalIntensity += shake.intensity * (shake.duration - ( Time.time - shake.startTime ) ) / shake.duration;
			}
		}
		
		Vector3 originalPos = transform.position - shakeOffset;
		Vector3 newOffset = new Vector3( Random.Range( -1 * totalIntensity, totalIntensity ), Random.Range( -1 * totalIntensity, totalIntensity ), Random.Range( -1 * totalIntensity, totalIntensity ) );
		
		transform.position = originalPos + newOffset;
		
		shakeOffset = newOffset;
	}
}