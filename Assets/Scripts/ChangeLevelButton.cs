﻿using UnityEngine;
using System.Collections;

public class ChangeLevelButton : MonoBehaviour {

	public int offset = 1;

	public void Clicked() {
		Application.LoadLevel( Application.loadedLevel + offset );
	}
}
